const express = require('express');
const fakeTags = require('../data/tags');


const router = express.Router();


// Get a list of posts
router.get('/', (req, res) =>
{
	res.json(fakeTags);
});


// Get a single post
router.get('/:id', (req, res) =>
{
	// Find the post in the array that has the id given by req.params.id
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find
	const tagId = Number(req.params.id);
	const foundTag = fakeTags.find((tag) => tag.id === tagId);
	if (!foundTag) {
		return res.status(404).json({
			error: 'Post not found',
		});
	}
	return res.json(foundTag);
});


module.exports = router;